<?php
/* Template Name: Logout*/

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<!-- Latest compiled and minified CSS -->
<div class="pad-top gray-back">
      <div class="content-fluid padding0 ">
             <div class=" banimgOne"><img src="<?php bloginfo('template_directory'); ?>/images/carosel/home-5.jpg" class="img-responsive"></div>
       </div>

	<div id="primary" class="container">
	<?php

		// Start the loop.
		while ( have_posts() ) : the_post();

		the_content();
		// End the loop.
		endwhile;
		?>

	
	</div><!-- .content-area -->

	</div>


<?php get_footer(); ?>
