<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
<link rel="shortcut icon" href="<?php  bloginfo('template_directory'); ?>/favicon.ico" />
	
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">	

<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/swiper.min.css">	
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/bootstrap.min.css" media="all" type="text/css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/" media="all" type="text/css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/style.css" media="all" type="text/css" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/custom.css" type="text/css" media="all" />
<link href='https://fonts.googleapis.com/css?family=Lato:400,300,300italic,400italic,700,700italic,900,900italic,100italic,100' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/bootstrap.min.js">  
</script>

<style>



    /* Note: Try to remove the following lines to see the effect of CSS positioning */
    .affix {
        z-index: 1;

    }

    .affix + .container-fluid {
        padding-top: 70px;
    }
</style>
</head>
<body>

<header id="navigation-bar" class="navbar-fixed-top  navigationbar">

    <div class="container">
        <!--Header Starts-->
        <div class="row navigationbar ">
            <div class="col-md-5 col-xs-2 pad-row">
              <a href="<?php echo esc_url( home_url( '/home' ) ); ?>" rel="home" >  <img src="<?php bloginfo('template_directory'); ?>/images/logo.png" alt="logo" id="logo" class="logo img-responsive"/> </a>
            </div>
            <div class="col-md-7 col-xs-10  mobile-pad padd0">
                
                <div class="row" >
                        <div class=" pad-row">
                            <div class="col-sm-5 ">
                                <div id="phonenum" >
                                    <div class="phonenumimg" >
                                      <p class="phone-area"><span> <img src="<?php bloginfo('template_directory'); ?>/images/call.jpg" alt="phone" /></span>
                                  
                                       <span class="span-num"> +94 770 067 774 </span></p> 
                                    </div>
                                </div>​
                            </div>
                            <div class="col-sm-3 col-xs-6 mobile-pad0">
                                <div id="listyourproperty">
                                  
                                    <div class="listyourpropertytxt">

    <?php  
            if ( is_user_logged_in() ) {
         echo '<a href="'; echo esc_url( home_url( '/list-your-property-with-us' ) ); echo '"> <img src="';   bloginfo('template_directory')?><?php echo '/images/plus.jpg" class="listyourpropertyimg" alt="list" />Submit </a>';  ?>                                            
                                                <?php
                } else {
          echo '<a href=" '; echo esc_url( home_url( '#' ) ); ?><?php echo '"><img src="'; bloginfo('template_directory')?><?php echo '/images/plus.jpg" class="listyourpropertyimg" alt="login" />List your property with us </a>'; }?>
                                         


                                       
                                    </div>
                                </div>​
                            </div>
                            <div class="col-sm-2 col-xs-3 mobile-pad0">
                                <div id="login" >
                                  
                                    <div class=" logintxt">
                                         <?php  
            if ( is_user_logged_in() ) {
         echo '<a href="'; echo esc_url( home_url( '/profile' ) ); echo '"> <img src="';   bloginfo('template_directory')?><?php echo '/images/login.jpg" class="loginimg" alt="login" />Profile </a>';  ?>                                            
                                                <?php
                } else {
          echo '<a href=" '; echo esc_url( home_url( '/login' ) ); ?><?php echo '"><img src="'; bloginfo('template_directory')?><?php echo '/images/login.jpg" class="loginimg" alt="login" />Login </a>'; }?>
                                         
                                     </div>   
                                    
                                </div>
                              
                            </div>
                            <div class="col-sm-2 col-xs-3 mobile-pad0">
                                <div id="register-header" >
                                   
                                    <div class="registertxt" >

   <?php  
            if ( is_user_logged_in() ) {
         echo '<a href="'; echo wp_logout_url( home_url() ); echo '"> <img src="';   bloginfo('template_directory')?><?php echo '/images/register.jpg" class="registerimg" alt="login" />Logout </a>';  ?>                                            
                                                <?php
                } else {
          echo '<a href=" '; echo esc_url( home_url( '/register' ) ); ?><?php echo '"><img src="'; bloginfo('template_directory')?><?php echo '/images/register.jpg" class="registerimg" alt="login" />Register </a>'; }?>
                                         


                                        
                                    </div>
                                </div>
                            </div>
                    </div>
                </div>
                
                                    <div class="col-md-2 navxxx" id="btnnav" >
                                        <div id="show" style="cursor: pointer;" > 
                                            <img src="<?php bloginfo('template_directory'); ?>/images/Menu.png" alt="navigation"class="img-responsive" />
                                         </div> 
                                          <div id="hide" style="cursor: pointer;"   >
                                            <img src="<?php bloginfo('template_directory'); ?>/images/Close.png" class="img-responsive" />
                                          </div>
                                    </div>
                                   
                                        <div class="row2" id="navxxx"  data-spy="affix"  >
                                            <nav class="navbar2 navbar-center">
                                               
                                                    <div id="navbar2" class="navbar-collapse collapse">
                                                                            
                                                    <?php /* Primary navigation */
                                                     wp_nav_menu( array(
                                                     'menu' => 'main_menu',
                                                     'depth' => 2,
                                                     'container' => false,
                                                      'menu_class' => 'nav navbar-nav navbar-sub',
                                                  //Process nav menu using our custom nav walker
                                                 // 'walker' => new wp_bootstrap_navwalker()
                                                    )
                                                    );
                                                    ?>
                                   
                                                </div>
                                            </nav>
                                        </div>
   <!-- Fixed navbar --> 
     <nav id="mynav" class="navbar navbar-default navbar-right main-nav <?php 
     if (!is_user_logged_in()) { echo "custom-nav-out ";}?>custom-nav">
                          <div class="">
                            <div class="navbar-header">
                              <button type="button" class="navbar-toggle collapsed mobile-menu-size" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                              </button>
                              
                            </div>
                            <div id="navbar" class="nav-bar-change navbar-collapse collapse">
                              <!--<ul class="nav navbar-nav">-->
                              <!--  <li class="active"><a href="#">Home</a></li>-->
                              <!--  <li><a href="#about">About</a></li>-->
                              <!--  <li><a href="#contact">Contact</a></li>-->
                              <!--  <li class="dropdown">-->
                              <!--    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>-->
                              <!--    <ul class="dropdown-menu">-->
                              <!--      <li><a href="#">Action</a></li>-->
                              <!--      <li><a href="#">Another action</a></li>-->
                              <!--      <li><a href="#">Something else here</a></li>-->
                              <!--      <li role="separator" class="divider"></li>-->
                              <!--      <li class="dropdown-header">Nav header</li>-->
                              <!--      <li><a href="#">Separated link</a></li>-->
                              <!--      <li><a href="#">One more separated link</a></li>-->
                              <!--    </ul>-->
                              <!--  </li>-->
                              <!--</ul>     
                              -->
    <?php /* Primary navigation */
     wp_nav_menu( array(
     'menu' => 'main_menu',
     'depth' => 2,
     'container' => false,
      'menu_class' => 'nav navbar-nav def-nav',
  //Process nav menu using our custom nav walker
 // 'walker' => new wp_bootstrap_navwalker()
    )
    );
    ?>
                            </div><!--/.nav-collapse -->
                          </div> 
                        </nav>
                </div>
            </div>
      </div>
</header>
<?php
// 	if(isset($_POST['btn_search'])){
// 		$proptype = $_POST['proptype'];
// 		$location = $_POST['location'];
// 		$beds = $_POST['beds'];
// 		$bathrooms = $_POST['bathrooms'];
// 		$sq_ft = $_POST['sq_ft'];
// 		$price = $_POST['price'];
// 		$args = array();

// 		$args = array(
// 			'tax_query' => array(
// 				post_type => 'property',
// 				'relation' => 'AND',
// 				array(
// 				'taxonomy' => 'property_type',
// 				'field' => 'slug',
// 				'terms' => array( $proptype )
// 				),
// 				array(
// 				'taxonomy' => 'location',
// 				'field' => 'slug',
// 				'terms' => array( $location )
// 				),
// 			)
// 		);	
		//print_r($args);exit;
		
// 		$args = array(
// 			'tax_query' => array(
// 				post_type => 'property',
//         		  array('relation' => 'AND',
//                       array( 'taxonomy' => 'propty_type',
//         			'field' => 'slug',
//         			'terms' => $proptype,
//         			'operator' => 'IN',
//         			),
//                       array( 'taxonomy' => 'location',
//         			'field' => 'slug',
//         			'terms' => $location,
//         			'operator' => 'IN'
//         			)
//         	      )
// 			)
// 		);		

		
		
		
		
		
		
		    // the query
    // $the_query = new WP_Query( $args );

    // if ( $the_query->have_posts() ) :
    //     while ( $the_query->have_posts() ) : $the_query->the_post();
    //     $image = get_field('images');
    //     echo $image;
    //     //exit;
    //     endwhile;
    // endif;
    
//	}
?>
               