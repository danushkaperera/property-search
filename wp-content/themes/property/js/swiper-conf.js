 
   
    var width = $(window).width();
    if(width < 480){ //mobile
    	 var swiper = new Swiper('.swiper-container', {
      nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 1,
        slidesPerColumn: 2,
        paginationClickable: true,
        spaceBetween: 30
    });
    
    }else if(width > 480 && width <= 769){ //tab
    	 var swiper = new Swiper('.swiper-container', {
       nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 2,
        slidesPerColumn: 2,
        paginationClickable: true,
        spaceBetween: 30
    });
    
    }else if(width > 769){
    	
    	 var swiper = new Swiper('.swiper-container', {
      nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 3,
        slidesPerColumn: 2,
        paginationClickable: true,
        spaceBetween: 30
    });
    
    }
    // var swiper = new Swiper('.swiper-container', {
    //     pagination: '.swiper-pagination',
    //     slidesPerView: 3,
    //     slidesPerColumn: 1,
    //     paginationClickable: true,
    //     spaceBetween: 30
    // });
    
     $("#show").click(function(){
        $("#show").hide();
         $("#hide").show();
         $('.navbar2').css({'display': 'block'});
    //    $("#navxxx").toggle("slow", "linear");
    });
    $("#hide").click(function(){
        $("#show").show();
         $("#hide").hide();
         $('.navbar2').css({'display': 'none'});
    });