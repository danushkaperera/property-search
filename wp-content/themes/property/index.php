<?php
/* 


Template Name: Home-Page

**/
$post_4 = get_post(4); 
$title = $post_4->post_title;
$content = $post_4->post_content;
get_header();
?>
<head>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/search-bar.js"></script>


 

    <!-- Demo styles -->
    <style>

.home-text1{
	padding-left: 5em;
	padding-right: 7em;
}

@media (max-width: 1024px) and (min-width: 768px){
		.pad-top {
		    padding-top: 129px !important;
		}
	}

@media(min-width:320px) and (max-width: 700px){
     .back-white{
    /*height: 1060px;*/
    }
     .move-prev {
    left: 12px !important;
    top:40% !important;
   }
   .move-next {
    right:22px !important;
     top:40% !important;
   }
}
     .move-next,.move-prev{
    top: 60%;
    }
    
    .main-nav{
    background-color: transparent !important;
    }
 
    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background: #fff;
        height: 527px;
     
    }

    @media all and (-webkit-min-device-pixel-ratio:0) and (min-resolution: .001dpcm) {
   .custom-nav-out {
        margin-top: 0px !important;
    }

}
  
   @media screen and (-webkit-min-device-pixel-ratio:0) {
    ::i-block-chrome,  .custom-nav-out {
   /* Add your style*/
     margin-top: -12px !important;
    }
}

    </style>


</head>

<div class="pad-top ">
      <div class="content-fluid padding0 ">
             <div class=" banimgOne">
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
								<div class="carousel-inner " role="listbox">
									<div class="item active vertpan item-1 ">
										<img src="<?php bloginfo('template_directory'); ?>/images/carosel/banner_home.jpg" alt="baner" class="img-responsive">
										<div class="carousel-caption caption-right caption1">
											
										</div>
									</div>
					
									<div class="item vertpan ">
										<img src="<?php bloginfo('template_directory'); ?>/images/carosel/banner_home.jpg" alt="baner" class="img-responsive">
										<div class="carousel-caption caption-left caption2">
											
										</div>
									</div>
					
									<div class="item vertpan">
										<img src="<?php bloginfo('template_directory'); ?>/images/carosel/banner_home.jpg" alt="baner" class="img-responsive">
										<div class="carousel-caption caption-left caption3">
											
										</div>
									</div>
								</div>
				            </div>
             	</div>
           </div>
   </div>
<div class="container-fluid back-white " >
	<div class="container " >
		
		
		      <div class="row search-bar">
		      	  <div class="col-md-12 ">
		                <div id="bigsearch" class=" bigcol " >
		               
		                   <div id="search-logo" class="col-sm-2 findlogo ">   <a  href="<?php echo esc_url( home_url( '/home' ) ); ?>" rel="home" >
		                    <img src="<?php bloginfo('template_directory'); ?>/images/findprop.png" class="img-responsive findlogoblue" alt="findlogo"> </a>
		                   </div>
		                  
		                   <div class="col-sm-10 formsearch">  
							                 <form role="search" class="search-row" method="POST" id="searchform" action="<?php echo home_url( '/search-result' ); ?>">
							                  <?php
							                    $terms_proptype = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'property_type',
							                        'hide_empty' => false,
							                        ) );
							                        
							                          $terms_location = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'location',
							                        'hide_empty' => false,
							                        ) );
							                        
							                        
							                          $terms_beds = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'beds',
							                        'hide_empty' => false,
							                        ) );
							                        
							                        
							                        //   $terms_bathrooms = get_terms( array(
							                        // 'post_type' => 'property',
							                        // 'taxonomy' => 'bathrooms',
							                        // 'hide_empty' => false,
							                        // ) );
							                        
							                        
							                        //   $terms_sq_ft = get_terms( array(
							                        // 'post_type' => 'property',
							                        // 'taxonomy' => 'sq_ft',
							                        // 'hide_empty' => false,
							                        // ) );
							                        
							                        
							                          $terms_price = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'price',
							                        'hide_empty' => false,
							                        ) );
							                        
							                        
							                        
							                        
							                   ?>
							                   
							                   
							                  		<div class=" row">		
														<div  class="col-sm-2 five-cols search-padd">
															<label class="lbl">Property Type</label>
														    <select class="form-control height-fild padd0" name="proptype" >
														      <option value="">any</option>
														     <?php  foreach($terms_proptype as $term ){   ?>
														      <option value="<?php echo $term->name; ?>" ><?php echo $term->name; ?></option>
														      <?php } ?>
														    </select>
							                  		    </div>
							                  		    <div  class="col-sm-2 five-cols search-padd">
							                  		    	<label class="lbl">Location</label>
														    <select class="form-control height-fild padd0"  name="location" >
														      <option value="">any</option>
														     <?php  foreach($terms_location as $term2 ){   ?>
														      <option value="<?php echo $term2->name; ?>"><?php echo $term2->name; ?></option>
														      <?php } ?>
														    </select>
							                  			</div>
							                  			  <div  class="col-sm-2 five-cols search-padd">
							                  			  	<label class="lbl">Beds</label>
														    <select class="form-control height-fild padd0" name="beds" >
														      <option value="">any</option>
														     <?php  foreach($terms_beds as $term3 ){   ?>
														      <option value="<?php echo $term3->name; ?>" ><?php echo $term3->name; ?></option>
														      <?php } ?>
														    </select>
							                  			</div>
							                  		<!-- 	  <div  class="col-sm-2 seven-cols search-padd">
							                  			  	<label class="lbl">Bath</label>
														    <select class="form-control height-fild padd0" name="bathrooms">
														      <option value="">any</option>
														     <?php // foreach($terms_bathrooms as $term4 ){   ?>
														      <option value="<?php// echo $term4->name; ?>" ><?php //echo $term4->name; ?></option>
														      <?php// } ?>
														    </select>
							                  			</div> -->
							                  <!-- 			  <div  class="col-sm-2 seven-cols search-padd">
							                  			  	<label class="lbl">Sq ft</label>
														    <select class="form-control padd0 height-fild" name="sq_ft">
														      <option value="">any</option>
														     <?php // foreach($terms_sq_ft as $term5){   ?>
														      <option value="<?php // echo $term5->name; ?>" ><?php //echo $term5->name; ?></option>
														      <?php// } ?>
														    </select>
							                  			</div> -->
							                  		  <div  class="col-sm-2 five-cols search-padd">
							                  		  	   <label class="lbl">Price</label>
														    <select class="form-control padd0 height-fild" name="price">
														      <option value="">any</option>
														     <?php  foreach($terms_price as $term6 ){   ?>
														      <option value="<?php echo $term6->name; ?>" ><?php echo $term6->name; ?></option>
														      <?php } ?>
														    </select>
							                  			</div>
							                  		<div  class="col-sm-2 five-cols search-padd">
							                  			  <label class="lbl"></label>
							                  		 <input name="btn_search" type="submit" class="btn btn-primary" id="searchsubmit" value="Search" />
							                  	
							                  	  	</div>
							                    </div>
							                  	</form>	
		                  
		                            
		                </div> 
	              </div>  
 			</div> 
			  <div id="main-page-text" class="main-text container  ">
			             <h2 class="bold-txt">FIND YOUR PERFECT PROPERTY</h2>
			             <?php echo $content; ?>			
			  </div>
		     </div>
		</div>
	</div>
<div class="container-fluid padd0 gray-back padd-prop">
	<div class="container " >	 
	  <div class="row pad-bot">
	  	<div class="col-md-12">
	   					<p class="fproperty">FEATURED PROPERTIES</p>
	 		<p class="blueline"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/blueunderline.jpg"/></p>
			<div class="swiper-container">
	             <div class="swiper-wrapper" id="my-search">
	                    	
	                    	

					         <?php
					              $arg_main = array(
					            		'tax_query' => array(
										'post_type' => 'property',
										array(
										'taxonomy' => 'rent_or_buy',
										'field' => 'slug',
										'terms' => array( 'featured' )
										),
											array (									
									          'taxonomy' => 'property_type',
									          'terms' => array (),
										      'field' => 'slug',
										      'operator' => 'NOT IN',
										),
									)
					            );
					            // the query
					            $the_query_main = new WP_Query($arg_main);
					
					            if ( $the_query_main->have_posts() ) :
					            while ( $the_query_main->have_posts() ) : $the_query_main->the_post();
					        	$args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
					            $term = wp_get_post_terms( $post->ID, 'property_type', $args );
					            ?> 
	            <div class="swiper-slide ">

	            <?php 	if($term[0]->slug == 'land'){?>
	<div class="property-image">
								    <a href="<?php echo the_permalink();?>">
								    	<img src="<?php echo the_field('images');?>" class="img-responsive">
								    </a>
								</div>
						<div class="feature-home-prop">
									<div class="purpose <?php echo the_field('rent_buy'); ?>">
									<p class="buy-rent "><?php echo the_field('rent_buy'); ?></p>
									</div>
							<div class="propdetail">
									<div class="address">
										<p class="txt-address address"> 
										<?php echo the_field('address'); ?>								
										</p>
									</div>
									
								<div class="price">
								<p class="txt-price"><?php echo the_field('price'); ?> </p>
								</div>
									
									<div class="row land-space">
										<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
											  <p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/home.jpg">
											   <span class="items-prop gray-color-font">Area</span>
											  </p>
											</div>
											<div class="col-xs-6 prop-item">
												<p class="items-txt"><?php echo the_field('lot_size'); ?></p>
											</div>
										</div>
									</div>
									<div class="row">
	                              		<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
											     <p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/water.png">
										    	<span class="items-prop gray-color-font">Water</span> </p>
										    </div>
									        <div class="col-xs-6 prop-item">
								        		<p class="items-txt gray-color-font "><?php echo the_field('beds'); ?> </p>
									         </div>
									    </div>
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
												<p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/electricity.png">
												<span class="items-prop gray-color-font">Electricity</span> </p>
											</div>
											<div class="col-xs-6 prop-item">
												<p class=" items-txt gray-color-font"><?php echo the_field('bathrooms'); ?></p>
											</div>
										</div>
									</div>
									
	                               	<div class="col-xs-12 txt-address2">
									     <p class="gray-color-font" >
									      <img src="<?php bloginfo('template_directory'); ?>/images/carosel2/locationmark.jpg" /> <span><?php echo the_field('address'); ?> </span>
									      </p>
									</div>
                              </div>									
						</div>

<?php }else{?>
								<div class="property-image">
								    <a href="<?php echo the_permalink();?>">
								    	<img src="<?php echo the_field('images');?>" alt="property-image" class="img-responsive">
								    </a>
								</div>
				  <div class="feature-home-prop">
						<div class="purpose <?php echo the_field('rent_buy'); ?>"><p class="buy-rent "><?php echo the_field('rent_buy'); ?></p>
						</div>
                         <div class="propdetail">
								<div class="address">
								    <p class="txt-address address">						     
										<?php echo the_field('address'); ?>
									 </p>
								</div>
								<div class="price">
										<p class="txt-price">
											<?php echo the_field('price'); ?> 
										</p> 
								</div>
								<div class="row">
										<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
											  <p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/home.jpg" alt="sq-ft">
											   <span class="items-prop gray-color-font">Square Feet</span>
											  </p>
											</div>
											<div class="col-xs-6 prop-item">
												<p class="items-txt"><?php echo the_field('lot_size'); ?></p>
											</div>
										</div>
									</div>
									
	                              	<div class="row">
	                              		<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
											     <p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/car.jpg" alt="bed">
										    	<span class="items-prop gray-color-font">Beds</span> </p>
										    </div>
									        <div class="col-xs-6 prop-item">
								        		<p class="items-txt gray-color-font "><?php echo the_field('beds'); ?> </p>
									         </div>
									    </div>
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
												<p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/bathroom.jpg">
												<span class="items-prop gray-color-font">Bathrooms</span> </p>
											</div>
											<div class="col-xs-6 prop-item">
												<p class=" items-txt gray-color-font"><?php echo the_field('bathrooms'); ?></p>
											</div>
										</div>
									</div>
									
	                               <div class="col-xs-12 txt-address2">
									   <p class="gray-color-font" > 
									     <img src="<?php bloginfo('template_directory'); ?>/images/carosel2/locationmark.jpg"/> <span><?php echo the_field('address'); ?> </span></p>
									</div>
                                  </div>									
						</div>
							<?php }?>
				</div>
	                    <?php endwhile;?>
	            <?php endif;?>
				</div>
		</div>
					  <a class=" move-prev"><img src="<?php bloginfo('template_directory'); ?>/images/left-arrow.png"/></a>
                       <a class=" move-next "> <img src="<?php bloginfo('template_directory'); ?>/images/right-arrow.png"></a>
	   </div>	
	</div>	    
 </div>	 
 </div>	
 

  <!-- Swiper JS -->
    <script src="<?php bloginfo('template_directory'); ?>/js/swiper.jquery.js"></script>
 <!--<script src="<?php // bloginfo('template_directory'); ?>/js/swiper.min.js"></script>-->

    <!-- Initialize Swiper -->
    <script>
    var width = $(window).width();
    if(width < 480){ //mobile
    	 var swiper = new Swiper('.swiper-container', {
     //   pagination: '.swiper-button-next',
    	nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 1,
        slidesPerColumn: 1,
        paginationClickable: true,
        spaceBetween: 10,
       //  loop: true
    });
    
    }else if(width > 480 && width <= 769){ //tab
    	 var swiper = new Swiper('.swiper-container', {
     //   pagination: '.swiper-button-prev .swiper-button-next',
      nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 2,
        slidesPerColumn: 1,
        paginationClickable: true,
        spaceBetween: 30,
     //    loop: true
    });
    
    }else if(width > 769){
    	
    	 var swiper = new Swiper('.swiper-container', {
     //   pagination: '.swiper-button-next .swiper-button-prev',
      //  pagination: '.swiper-button-prev',
       nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 3,
        slidesPerColumn: 1,
        paginationClickable: true,
        spaceBetween: 30,
       //  loop: true
    });
    
     
    
    }
  
    </script>
<?php get_footer(); ?>
