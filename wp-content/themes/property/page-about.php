<?php
/* Template Name: About Us*/

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
$post_23 = get_post(23); 
$title = $post_23->post_title;
$content = $post_23->post_content;

get_header(); ?>
 <style>
 

@media(min-width:320px) and (max-width: 700px){
 .back-white{
    height: auto!important;
    }
 }
    
    
   
    body{
    background-color:#fff;
    }
  </style>
<!-- Latest compiled and minified CSS -->
<div class="pad-top gray-back">
      <div class="content-fluid padding0  ">
             <div class=" banimgOne2"><img src="<?php echo get_field( 'banners',23 ); ?>" class="img-responsive"></div>
       </div>
 <div class="content-fluid padding0 back-white ">
	<div id="primary" class="container">
	      
   <div class="col-md-12 content-about"> 					
                            <p class="fproperty bold-txt">ABOUT US</p>
	 		          <p class="blueline">
         <img src="<?php bloginfo('template_directory'); ?>/images/carosel2/blueunderline.jpg"></p>
<?php echo $content; ?>
</div>
     <p class="fproperty bold-txt">BIO</p>
<p class="blueline"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/blueunderline.jpg"></p>
<div class="row pading20">
  <div class="col-md-12 pad-about-us">
      <div class="col-md-6">
        <img src="<?php echo get_field( 'profile_image1',23 ); ?>" alt="noa" class="img-responsive">
      </div>
      <div class="col-md-6">
        <p class="strong-txt2  ">Ms. Noa Fernando</p>
        <p class="blueline2"><img src="<?php bloginfo('template_directory'); ?>/images/long-undrline.png"></p>
<p  class="content-text2 "><?php echo get_field( 'profile1_descript',23 ); ?></p>
      </div>
      
   </div>
   <div class="col-md-12 pad-about-us">
      <div class="col-md-6">
       
         


    <p class="strong-txt2 ">Meet My Business Partners</p>
 <p class="blueline2"><img src="<?php bloginfo('template_directory'); ?>/images/long-undrline.png"></p>
<p class="content-text2 "> <?php echo get_field( 'profile2_descript',23 ); ?></p>

      </div>
      <div class="col-md-6">
         <img src="<?php echo get_field( 'profile_image2',23 ); ?>"  class="img-responsive teamimage" alt="team">
      </div>
      
   </div>
</div>
 
	 </div> 
	</div><!-- .content-area -->
<div class=""></div>
	</div>
 </div> 

<?php get_footer(); ?>
