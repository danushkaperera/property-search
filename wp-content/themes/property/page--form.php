<?php
/* Template Name: form*/
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<div class="container-fluid pad-top ">
    <div class="row pad-row">
        <div class=" banimgOne"><img src="<?php bloginfo('template_directory'); ?>/images/carosel/home-5.jpg" class="img-responsive"></div>

    </div>

</div>
<div class="container">
    <div class="row listProperty">
        <div class="col-md-3">
            <p>LIST PROPERTY</p>

        </div>
    </div>

</div>
<div class="container-fluid bodyform" >


    <div class="container inttroform">
        <div class="row">
            <div class="col-md-12">

                <form role="form" >
                    <div class="row titleform">
                        <div class="col-md-12">
                            <p class="formain">Main Information</p>
                            <p class="formsub">Lorem Ipsum is simply dummy text of the printing and type setting industry</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="hrline02"></div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12">
                            <label class="control-label" for="pTitle">Property Title:</label>
                            <input class="form-control"  type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-3" >
                            <label class="control-label" for="pTitle">Currency</label>
                            <input class="form-control"  type="text">
                        </div>
                        <div class="form-group col-md-2">
                            <label class="control-label" for="pTitle">Price</label>
                            <input class="form-control"  type="text">
                        </div>
                        <div class="form-group col-md-7">
                            <label class="control-label" for="pTitle">Price Format</label>
                            <input class="form-control"  type="text">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-12" >
                            <label class="control-label" for="pTitle">Description</label>
                            <textarea class="form-control" rows="5" id="comment"></textarea>
                        </div>
                    </div>
                    <div class="row titleform">
                        <div class="col-md-12">
                            <p class="formain">Summary</p>
                            <p class="formsub">Lorem Ipsum is simply dummy text of the printing and type setting industry</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="hrline02"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="form-group col-md-6" >
                                    <label class="control-label" for="pTitle">Conditions</label>
                                    <div class="dropdown"  >
                                            <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%; text-align: right;">

                                                <span class="caret" ></span>
                                            </button>
                                            <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="pTitle">Price</label>
                                    <div class="dropdown"  >
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%; text-align: right;">

                                            <span class="caret" ></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group col-md-4" >
                                    <label class="control-label" for="pTitle">Rooms</label>
                                    <div class="dropdown"  >
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%; text-align: right;">

                                            <span class="caret" ></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="pTitle">Beds</label>
                                    <div class="dropdown"  >
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%; text-align: right;">

                                            <span class="caret" ></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="pTitle">Bathrooms</label>
                                    <div class="dropdown"  >
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%; text-align: right;">

                                            <span class="caret" ></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-4" >
                                    <label class="control-label" for="pTitle">Garages</label>
                                    <div class="dropdown"  >
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%; text-align: right;">

                                            <span class="caret" ></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="pTitle">Area:</label>
                                    <input class="form-control"  type="text">
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="pTitle">Gallery Images</label>
                                    <a href="#"><p style="background-color: #00aeef; color: white; text-align: center "><img class="img-responcive" src="img/form/1.png" >ADD MORE</span></p></a>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="uloaded" style="background-color: grey">
                                <br>
                                <p style="text-align: center"><img src="img/form/2.png"></p>
                                <p style="text-align: center">Add Featured Photo</p>
                                <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing</p>
                                <a href="#"><p style="text-align: center"><img src="img/form/3.png"></p></a>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="row titleform">
                        <div class="col-md-12">
                            <p class="formain">Main Information</p>
                            <p class="formsub">Lorem Ipsum is simply dummy text of the printing and type setting industry</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="hrline02"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="control-label" for="pTitle">Property Title:</label>
                                    <input class="form-control"  type="text">
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-12">
                                    <label class="control-label" for="pTitle">Property Title:</label>
                                    <input class="form-control"  type="text">
                                </div>
                            </div>

                            <div class="row">
                                <div class="form-group col-md-6" >
                                    <label class="control-label" for="pTitle">Conditions</label>
                                    <div class="dropdown"  >
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%; text-align: right;">

                                            <span class="caret" ></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <label class="control-label" for="pTitle">Price</label>
                                    <div class="dropdown"  >
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%; text-align: right;">

                                            <span class="caret" ></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <div class="row">
                                <div class="form-group col-md-4" >
                                    <label class="control-label" for="pTitle">Rooms</label>
                                    <div class="dropdown"  >
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%; text-align: right;">

                                            <span class="caret" ></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="pTitle">Beds</label>
                                    <div class="dropdown"  >
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%; text-align: right;">

                                            <span class="caret" ></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label class="control-label" for="pTitle">Bathrooms</label>
                                    <div class="dropdown"  >
                                        <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true" style="width:100%; text-align: right;">

                                            <span class="caret" ></span>
                                        </button>
                                        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                                            <li><a href="#">Action</a></li>
                                            <li><a href="#">Another action</a></li>
                                            <li><a href="#">Something else here</a></li>
                                            <li role="separator" class="divider"></li>
                                            <li><a href="#">Separated link</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-6">
                            <div class="uloaded" style="background-color: grey">
                                <br>
                                <p style="text-align: center"><img src="<?php bloginfo('template_directory'); ?>/images/form/2.png"></p>
                                <p style="text-align: center">Add Featured Photo</p>
                                <p style="text-align: center">Lorem Ipsum is simply dummy text of the printing</p>
                                <a href="#"><p style="text-align: center"><img src="<?php bloginfo('template_directory'); ?>/images/form/3.png"></p></a>
                                <br>
                            </div>
                        </div>
                    </div>
                    <div class="row titleform">
                        <div class="col-md-12">
                            <p class="formain">Main Information</p>
                            <p class="formsub">Lorem Ipsum is simply dummy text of the printing and type setting industry</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="hrline02"></div>
                    </div>
                    <div class="row">
                        <div class="col-md-8">
                            <div class="row">
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value=""><span></span></label><label for="test">24 Hour Security</label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value=""><span></span></label><label for="test">Cablt TV</label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value=""><span></span></label><label for="test">Hot Water</label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value="" ><span></span></label><label for="test">Swimming Pool</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value=""><span></span></span></label><label for="test">AC</label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value=""><span></span></label><label for="test">CCTV System	</label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value="" ><span></span></label><label for="test">Fully Furnished</label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value="" ><span></span></label><label for="test">Kids Play Area</label>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value=""><span></span></label><label for="test">Brand Ne</label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value=""><span></span></span></label><label for="test">Gym</label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value=""><span></span></span></label><label for="test">Semi-furnished</label>
                                    </div>
                                    <div class="checkbox">
                                        <label class="myCheckbox"><input type="checkbox" value=""><span></span></span></label><label for="test">Balcony</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="button1">
                                <a href="#"><p style="background-color: #00aeef; color: white; text-align: center ">SUBMIT PROPERTY</p></a>
                                </a>
                            </div>
                        </div>

                    </div>

                </form>


            </div>
        </div>
    </div>
</div>

<!--owl carousel starts-->
<!--owl carousel ends-->

<?php get_footer(); ?>
