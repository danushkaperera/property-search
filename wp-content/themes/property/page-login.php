<?php
/* Template Name: login */

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>

<style>
[type="checkbox"]:not(:checked),
[type="checkbox"]:checked{

	position: relative;
    left: 0px;
}
</style>

<!-- Latest compiled and minified CSS -->
<div class="pad-top gray-back">
      <div class="content-fluid padding0 ">
          <div class=" banimgOne2"><img src="<?php bloginfo('template_directory'); ?>/images/about.png" class="img-responsive"></div>
       </div>

	<div id="primary" class="container padd-top-bot">
	<h3 class="bold-txt">LOGIN FORM</h3>
	
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

		the_content();
		// End the loop.
		endwhile;
		?>

	</div>
</div>
<?php get_footer(); ?>
