<?php
/* Template Name: Search Result*/
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<style>
    .main-nav{
        background-color:#fff;
    }
</style>
<div class="container-fluid pad-top ">
    <div class="row ">
        <div class=" banimgOne"><img src="<?php bloginfo('template_directory'); ?>/images/carosel/banner_small_1.jpg" class="img-responsive"></div>

    </div>

</div>
<div class="container">
    <div class="row listProperty">
        <div class="col-md-3">
        
        </div>
    </div>
</div>
<div class="container-fluid bodyform" >
    <div class="container inttroform">
        <div class="row">
            <div class="col-md-12">

    <?php    
        
      	if(isset($_POST['btn_search'])){
		$proptype = $_POST['proptype'];
		$location = $_POST['location'];
		$beds = $_POST['beds'];
		$bathrooms = $_POST['bathrooms'];
		$sq_ft = $_POST['sq_ft'];
		$price = $_POST['price'];
		$args = array();


		$args = array(			
				'post_type' => 'property',
                'tax_query' => array(
                 //'relation' => 'OR',
				'relation' => 'AND',
	         //   'order'   => 'DESC',
    				// array(
    			    
    				    
    				       array(
            				'taxonomy' => 'property_type',
            				'field' => 'slug',
            				'terms' =>  $proptype,
                            'compare' => '=',
            				),
            				array(
            				'taxonomy' => 'location',
            				'field' => 'slug',
            				'terms' => $location,
                            'compare' => '=',
            				),
            					array(
            				'taxonomy' => 'beds',
            				'field' => 'slug',
            				'terms' => $beds,
                            'compare' => '=',
            				),
            				// 	array(
            				// 'taxonomy' => 'bathrooms',
            				// 'field' => 'slug',
            				// 'terms' => array( $bathrooms )
            				//),
            				// 		array(
            				// 'taxonomy' => 'bathrooms',
            				// 'field' => 'slug',
            				// 'terms' => array( $sq_ft )
            				// ),
            					array(
            				'taxonomy' => 'price',
            				'field' => 'slug',
            				'terms' => $price,
                            'compare' => '<'
            				),
            	//	),
			),
		);	
             
          //    print_r($args);
               	?>



	<p class="fproperty">SEARCH RESULT</p>
	 		<p class="blueline"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/blueunderline.jpg"></p>
				    <div class="swiper-container">
	                    <div class="swiper-wrapper" id="my-search">
	                    	
					         <?php
					          	// Start the loop.
                    $the_query = new WP_Query( $args );
                  //   print_r( $the_query);
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) : $the_query->the_post();
                        
                        ?> 
	            <div class="swiper-slide ">
							<div class="property-image">
								<a href="<?php echo the_permalink();?>">
								    <img src="<?php echo the_field('images');?>" class="img-responsive">
							    </a>
							</div>
								<div>
									<div class="purpose <?php echo the_field('rent_buy'); ?>"><p class="buy-rent "><?php echo the_field('rent_buy'); ?></p></div>
									<div class="address"><p class="txt-address"> <p > <img src="<?php bloginfo('template_directory'); ?>/images/carosel2/locationmark.jpg"></img>
									<span class="address"><?php echo the_field('address'); ?></span></p></div>
									<div class="price"><p class="txt-price"><?php echo the_field('price'); ?> </p> </div>
									
									<div class="col-xs-6 prop-item">
									<p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/bathroom.jpg">
									<span class="items-prop .gray-color-font">Square Feet</span> </p>
									</div>
									<div class="col-xs-6 prop-item">
									<p><?php echo the_field('lot_size'); ?></p>
									</div>
	
										<div class="col-xs-6 prop-item">
									<p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/home.jpg">
									<span class="items-prop gray-color-font">Beds</span> </p>
									</div>
									<div class="col-xs-6 prop-item">
								<p class="gray-color-font"><?php echo the_field('beds'); ?> </p></div>
									
									
									
									<div class="col-xs-6 prop-item">
									<p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/car.jpg">
									<span class="items-prop gray-color-font">Bathrooms</span> </p>
									</div>
									<div class="col-xs-6 prop-item">
								<p class="gray-color-font"><?php echo the_field('bathrooms'); ?></p> </div>
									
	                               	<div class="col-xs-12 txt-address2">
									     <p class="gray-color-font" > <img src="<?php bloginfo('template_directory'); ?>/images/carosel2/locationmark.jpg"></img> <span><?php echo the_field('address'); ?> </span></p>
									</div>
									
								</div>
							
							</div>
	                    <?php endwhile;?>
	
	            
					 	
						</div>
					</div>
				<a class=" move-prev"><img src="<?php bloginfo('template_directory'); ?>/images/left-arrow.png"></img></a>
                <a class=" move-next "> <img src="<?php bloginfo('template_directory'); ?>/images/right-arrow.png"></img></a>
				 </div>
			</div>	    
			<?php }else{
        echo  'Sorry, no posts were found' ;
        }?>
			<?php
		}else {
	echo  'do nothing' ;
			  } ?>
        </div>	 
   </div>










            </div>
        </div>
    </div>
</div>




  <!-- Swiper JS -->
    <script src="<?php bloginfo('template_directory'); ?>/js/swiper.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
    var width = $(window).width();
    if(width < 480){ //mobile
    	 var swiper = new Swiper('.swiper-container', {
     //   pagination: '.swiper-button-prev .swiper-button-next',
        slidesPerView: 1,
        slidesPerColumn: 1,
        paginationClickable: true,
        spaceBetween: 30
    });
    
    }else if(width > 480 && width <= 769){ //tab
    	 var swiper = new Swiper('.swiper-container', {
     //   pagination: '.swiper-button-prev .swiper-button-next',
        slidesPerView: 2,
        slidesPerColumn: 1,
        paginationClickable: true,
        spaceBetween: 30
    });
    
    }else if(width > 769){
    	
    	 var swiper = new Swiper('.swiper-container', {
      //  pagination: '.swiper-button-prev .swiper-button-next',
        slidesPerView: 3,
        slidesPerColumn: 1,
        paginationClickable: true,
        spaceBetween: 30
    });
    
    }
   
    </script>


<?php get_footer(); ?>
