<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); ?>
 
 
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>


 
 
 
   <style>
    @media (max-width: 767px) and (min-width: 320px){
   .simlr-addrs{
     font-size: 12px;
      }
.fproperty{
  padding-top: 0px !important;
}
   .address-larg{
    font-size: 15px!important;
    padding-top: 18px;
      }
.custom-nav {  
    position: relative;
    top:-86px !important;
    -webkit-transform: translate(0px, -45px);
    /*transform: translate(-50%, -50%);*/
}
  .swiper-container {
    width: 77%!important;
  }
    .content {
    height: 193px !important;
    }
 .price-wrapper{
 padding-left:2px !important;
 padding-right:2px !important;
 }

@media screen { @media (min-width: 0px) {} }
@media screen and (min-color-index:0) and(-webkit-min-device-pixel-ratio:0) 
{ @media {
    .span-num { 

        color:#fff; 
       

    }
    #mynav {
    top: -150px !important;
  }
}}

}
@media(min-width:768px) and (max-width: 1024px){
  .property-pad-single{
        padding-top: 129px!important;
  }
	 .navigationbar{
    	height:160px!important;
    }
   
}
   .navigationbar{
   	    height: 167px;
   }
   
     .main-nav{
        display:none;
    }
    .navxxx{
        display:block;
    }
    .search-bar {
    position: relative;
    top: 2px !important;
        
    }
    .logo{
        display:none;
    }

  /*.main-nav {*/
  /*  background-color: transparent !important;*/
  /*  }*/
   .pad-top {
    padding-top: 166px;
}
    </style>
 
<div class="pad-top property-pad-single">
<div class="container">
 <div class="row ">
		      	  <div class="col-md-12 ">
		                <div id="bigsearch" class=" bigcol2 " >
                      <a  href="<?php echo esc_url( home_url( '/home' ) ); ?>" rel="home" >
        		                   <div id="search-logo" class="col-sm-2 findlogo2 padd0">
        		                    <img src="<?php bloginfo('template_directory'); ?>/images/findprop.png" class="img-responsive" alt="findlogo">
        		                   </div>
                      </a>         
		                   <div class="col-sm-10 formsearch">  
							                 <form role="search" class="search-row" method="POST" id="searchform" action="<?php echo home_url( '/search-result' ); ?>">
							                  <?php
							                    $terms_proptype = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'property_type',
							                        'hide_empty' => false,
							                        ) );
							                        
							                          $terms_location = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'location',
							                        'hide_empty' => false,
							                        ) );
							                        
							                        
							                          $terms_beds = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'beds',
							                        'hide_empty' => false,
							                        ) );
							                        
							                        
							                        //   $terms_bathrooms = get_terms( array(
							                        // 'post_type' => 'property',
							                        // 'taxonomy' => 'bathrooms',
							                        // 'hide_empty' => false,
							                        // ) );
							                        
							                        
							                        //   $terms_sq_ft = get_terms( array(
							                        // 'post_type' => 'property',
							                        // 'taxonomy' => 'sq_ft',
							                        // 'hide_empty' => false,
							                        // ) );
							                        
							                        
							                          $terms_price = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'price',
							                        'hide_empty' => false,
							                        ) );
							                        
							                        
							                        
							                        
							                   ?>
							                   
							                   
							                  		<div class=" row">		
														<div  class="col-sm-2 five-cols search-padd">
															<label class="lbl">Property Type</label>
														    <select class="form-control height-fild padd0" name="proptype" >
														      <option value="">any</option>
														     <?php  foreach($terms_proptype as $term ){   ?>
														      <option value="<?php echo $term->name; ?>" ><?php echo $term->name; ?></option>
														      <?php } ?>
														    </select>
							                  		    </div>
							                  		    <div  class="col-sm-2 five-cols search-padd">
							                  		    	<label class="lbl">Location:</label>
														    <select class="form-control height-fild padd0"  name="location" >
														      <option value="">any</option>
														     <?php  foreach($terms_location as $term2 ){   ?>
														      <option value="<?php echo $term2->name; ?>"><?php echo $term2->name; ?></option>
														      <?php } ?>
														    </select>
							                  			</div>
							                  			  <div  class="col-sm-2 five-cols search-padd">
							                  			  	<label class="lbl">Beds</label>
														    <select class="form-control height-fild padd0" name="beds" >
														      <option value="">any</option>
														     <?php  foreach($terms_beds as $term3 ){   ?>
														      <option value="<?php echo $term3->name; ?>" ><?php echo $term3->name; ?></option>
														      <?php } ?>
														    </select>
							                  			</div>
<!-- 							                   <div  class="col-sm-2 five-cols search-padd">
  							                  <label class="lbl">Baths</label>
  														    <select class="form-control height-fild padd0" name="bathrooms">
  														      <option value="">any</option>
  														     <?php // foreach($terms_bathrooms as $term4 ){   ?>
  														      <option value="<?php //echo $term4->name; ?>" ><?php //echo $term4->name; ?></option>
  														      <?php //} ?>
  														    </select>
							                  	</div>
							                  			  <div  class="col-sm-2 seven-cols search-padd">
							                  			  	<label class="lbl">Sq ft</label>
														    <select class="form-control padd0 height-fild" name="sq_ft">
														      <option value="">any</option>
														     <?php // foreach($terms_sq_ft as $term5){   ?>
														      <option value="<?php //echo $term5->name; ?>" ><?php //echo $term5->name; ?></option>
														      <?php// } ?>
														    </select>
							                  			</div> -->
							                  		  <div  class="col-sm-2 five-cols search-padd">
							                  		  	   <label class="lbl">Price</label>
														    <select class="form-control padd0 height-fild" name="price">
														      <option value="">any</option>
														     <?php  foreach($terms_price as $term6 ){   ?>
														      <option value="<?php echo $term6->name; ?>" ><?php echo $term6->name; ?></option>
														      <?php } ?>
														    </select>
							                  		 </div>
							                  		<div  class="col-sm-2 five-cols search-padd">
							                  			  <label class="lbl"></label>
							                  		 <input name="btn_search" type="submit" class="btn btn-primary" id="searchsubmit" value="Search" />
							                  	    </div>
							                  	  </div>
							                  	</form>	
		                  
		                             </div>
		                    </div>
		       </div> 
	    </div>  </div> 





  
  <?php  $location=get_field('google_map');  ?>
  <?php //echo $location['lat'];?>
<div id="view1"> <script
src="https://maps.googleapis.com/maps/api/js">
</script>
<script>
var myCenter=new google.maps.LatLng(<?php echo $location['lat'];?>,<?php echo $location['lng'];?>);

function initialize()
{
var mapProp = {
  center:myCenter,
  zoom:15,
  mapTypeId:google.maps.MapTypeId.ROADMAP
  };

var map=new google.maps.Map(document.getElementById("googleMap"),mapProp);

var marker=new google.maps.Marker({
  position:myCenter,
  });

marker.setMap(map);
}

google.maps.event.addDomListener(window, 'load', initialize);
</script>
</div>

<div class="container-fluid gray-back"> 

<div id="googleMap" style="width:100%; height:380px;"></div>
 
  <div class="container" >  
        <div class="row back-clr under-line " >
             <div class="col-md-12 singlr-pad">
                   <div class="col-md-6">
                        <div class="single-prop-head"> 
                    	        <p class="addres-h2"> <?php   echo the_field('address');    ?></p>
         <p>	<img src="<?php bloginfo('template_directory'); ?>/images/carosel2/locationmark.jpg" style="float:left;"/> 
                    <span class="address"> <?php   echo the_field('address'); ?> </span> 
                    	         <span class="price-mode">
                    	   <?php  echo the_field('price');  ?>
                   </span></p> 
    </div>
                                       
                                        
                                        
     <section id="gallery" class="simplegallery">
             <div class="content">
             <?php 
             $img1 =get_field('images');
             $img2 =get_field('image2');
             $img3 =get_field('image3');
             $img4 =get_field('image4');
             if(!empty($img1)){
               echo '<img src="'.$img1.'" class="image_1" alt="" />';
              }
               if(!empty($img2)){
               echo '<img src="'.$img2.'" class="image_2" style="display:none" alt="" />';
              }
              if(!empty($img3)){
               echo '<img src="'.$img3.'" class="image_3" style="display:none" alt="" />';
              }
               if(!empty($img4)){
               echo '<img src="'.$img4.'" class="image_4" style="display:none" alt="" />';
              }
              ?>
            </div>

            <div class="clear"></div>
            <div class="thumbnail">
                  <?php if(!empty($img1)){
                          echo ' <div class="thumb">  <a href="#" rel="1">
                         <img src="'.$img1.'" id="thumb_1" alt="" />
                         </a>  </div>';
                      } if(!empty($img2)){
                          echo '<div class="thumb">    <a href="#" rel="2">
                         <img src="'.$img2.'" id="thumb_2" alt="" />
                         </a> </div>';
                      } if(!empty($img3)){
                          echo '<div class="thumb"><a href="#" rel="3">
                          <img src="'.$img3.'" id="thumb_3" alt="" />
                          </a></div>';
                      } if(!empty($img4)){
                        echo '<div class="thumb last"> <a href="#" rel="4">
                         <img src="'.$img4.'" id="thumb_4" alt="" />
                       </a></div>';
                      }?>
            </div>
  </section>
                                        
     <?php $term = wp_get_post_terms( $post->ID, 'property_type' );
  if($term[0]->slug == 'land'){?>

                
 <div class="price-wrapper col-xs-12">
       <div class="sq-ft col-xs-4">
          <p> <img src="<?php bloginfo('template_directory'); ?>/images/carosel2/home.png" style="float:left">
           <span><?php  echo the_field('lot_size'); ?>&nbsp&nbsp&nbsp Sq Ft</span></p>
       </div>                                                      
      <div class="bed col-xs-4"> 
                     <p><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/watr1.png">
                    <span><?php  echo the_field('beds'); ?> Water</span></p> 
       </div>                                               
      <div class="bath col-xs-4">
                 <p><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/elect.png">
                 <span><?php   echo the_field('bathrooms'); ?> Electricity</span></p>
      </div>           
</div>

  <?php }else{?>
                
 <div class="price-wrapper col-xs-12">
       <div class="sq-ft col-xs-4">
          <p> <img src="<?php bloginfo('template_directory'); ?>/images/carosel2/home.png" style="float:left">
           <span><?php  echo the_field('lot_size'); ?>&nbsp&nbsp&nbsp Sq Ft</span></p>
       </div>                                                      
      <div class="bed col-xs-4"> 
                     <p><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/car.png">
                    <span><?php  echo the_field('beds'); ?>&nbsp&nbsp&nbsp Beds</span></p> 
       </div>                                               
      <div class="bath col-xs-4">
                 <p><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/bathroom.png">
                 <span><?php   echo the_field('bathrooms'); ?>&nbsp&nbsp&nbsp Bathrooms</span></p>
      </div>           
</div>

    <?php } ?>                            
       
                    
                                    </div>
                                      <div class="col-md-6">
                                               <div class="real-estate-description-b">
                                                <p class="descrip"> DESCRIPTION </p>
                                                 	<?php
                                                		// Start the loop.
                                                		while ( have_posts() ) : the_post();
                                                
                                                		the_content();
                                                		// End the loop.
                                                		endwhile;
                                                		?>

                                                 </div>
                                              <div class="dre-property-more">
                                                <p class="feature">PROPERTY FEATURES</p>
                                                <ul class="list-block">
                 <?php       $fields= get_field('property_features');
                                            $count =0;
                   foreach( $fields as $field_name  ){
                    $count++;
                    		// get_field_object( $field_name, $post_id, $options )
                    		// - $value has already been loaded for us, no point to load it again in the get_field_object function
                    	//	$field = get_field_object($field_name, false, array('load_value' => false));
                        if($count < 15 && !empty($field_name)){
  echo '<li class="col-xs-6 list-none"><i class="fa fa-square" aria-hidden="true"></i>&nbsp' . $field_name . '</li>';		//	echo $value;                    
                    	}                                       
                                            //  print render($content['property_suitables']);
                        }                    ?>
                                            </ul>
                                    
                                    </div>
                                     <div style="padding-bottom:10px;">
  <a href="<?php echo esc_url( home_url( '/contact-us' ) ); ?>" class="enquery">
  <img src="<?php bloginfo('template_directory'); ?>/images/inquery.png" alt="inquire" />INQUIRE</a>  &nbsp 
  <a href="#" onclick="printpage()" class="printpage">
  <img src="<?php bloginfo('template_directory'); ?>/images/print.png" alt="print" />PRINT</a>
                                    </div>
                           </div><br/><br/><br/>
                    </div></div>
                  
                    
                    
       <div class="row pad-top pad-top2" > 
           <div class="col-md-12" >
               <p class="fproperty">SIMILAR PROPERTIES</p>
	 		<p class="blueline"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/blueunderline.jpg"></p>
              <div class="swiper-container">
	                    <div class="swiper-wrapper">
                             <?php
        	            $arg_main = array(
        	                'post_type' => 'property'
        	            );
        	            // the query
        	            $the_query_main = new WP_Query( $arg_main );
        	            if ( $the_query_main->have_posts() ) :
        	            while ( $the_query_main->have_posts() ) : $the_query_main->the_post();
                            ?>
                          <div class="swiper-slide  similar-item ">
                            <a href="<?php echo the_permalink();?>">  <img src="  <?php echo get_field('images');   ?>" class="img-responsive small-img"></a>
                              <div class="similar-rent-buy"> <p class="<?php echo the_field('rent_buy'); ?>"> <?php echo the_field('rent_buy'); ?></p></div>
                              <div class="col-md-12">
                                  <p class="address-larg"><?php echo get_field('address');   ?></p>
                              <div>
                                 <p class="simlr-addrs"> <img src="<?php bloginfo('template_directory'); ?>/images/carosel2/locationmark.jpg" class="loc-ico" />
                                  <span ><?php echo get_field('address');   ?></span> </div></p>
                              <div class="similar-price"><p> <?php echo get_field('price');   ?></p></div>
                              </div>
                          </div>
                             <?php endwhile;?>
	
	            <?php endif;?>
            					</div>
            				 </div>
            			<a class=" move-prev"><img src="<?php bloginfo('template_directory'); ?>/images/left-arrow.png"></img></a>
                        <a class=" move-next "> <img src="<?php bloginfo('template_directory'); ?>/images/right-arrow.png"></img></a>
			
                     </div> 
                </div>
        
</div></div>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <!-- Swiper JS -->
<script src="<?php bloginfo('template_directory'); ?>/js/swiper.min.js"></script>


<script>
    var width = $(window).width();
    if(width < 480){ //mobile
    	 var swiper = new Swiper('.swiper-container', {
        nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 1,
        slidesPerColumn: 1,
        paginationClickable: true,
        spaceBetween: 20
    });
    
    }else if(width > 480 && width <= 768){ //tab
    	 var swiper = new Swiper('.swiper-container', {
         nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 3,
        slidesPerColumn: 1,
        paginationClickable: true,
        spaceBetween: 30
    });
    
    }else if(width > 769){
    	
    	 var swiper = new Swiper('.swiper-container', {
        nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 4,
        slidesPerColumn: 1,
        paginationClickable: true,
        spaceBetween: 30
    });
    
    }
  $("#show").click(function(){
        $("#show").hide();
        $("#hide").show();
        $('.navbar2').css({'display': 'block'});
    //    $("#navxxx").toggle("slow", "linear");
    });
    $("#hide").click(function(){
        $("#show").show();
        $("#hide").hide();
        $('.navbar2').css({'display': 'none'});
    });
    </script>

<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/js/simplegallery.min.js"></script>
<script type="text/javascript">
    $(document).ready(function(){
        $('#gallery').simplegallery({
            galltime : 400,
            gallcontent: '.content',
            gallthumbnail: '.thumbnail',
            gallthumb: '.thumb'
        });
    });



function printpage() {
    window.print();
}

</script>
<?php get_footer(); ?>
