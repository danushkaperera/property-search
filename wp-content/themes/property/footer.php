<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the "site-content" div and all content after.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
?>

<footer class="footermainone" id="footid">
<div class="container-fluid ">
    <div class="container">
            <div class="row">
                <div class="col-md-2 col-sm-3">
                    <p class="footermain terms">USEFUL LINKS</p>
                    <a href="#" class="footersub">ADD YOUR LISTING</a><br>
                    <a href="#" class="footersub">INVESTMENT</a><br>
                   <a href="#" class="footersub">PRICING</a><br>
                    <a href="#" class="footersub">WANTED</a><br>
                    <a href="#" class="footersub">ABOUT US</a><br>
                    <a href="#"  class="footersub">CONTACT US</a><br>
                </div>
                <div class="col-md-4 col-sm-3">
                    <p class="footermain terms ">FEATURED PROPERTIES</p>
                  
                    <a href="#" class="footersub">Gregory’s Road, Col 7</a><br>
                    <a href="#" class="footersub">Lorem ipsum dolor sit amet, consectetur
                        adipiscing elit.</a><br>
                    <a class="footersub">Gregory’s Road, Col 7</a><br>
                </div>
                <div class="col-md-3 col-sm-3 tab-mid">
                    <p href="#" class="terms footermain">OUR NEWSLETTER</p>
                    <a href="#" class="footersub">Lorem ipsum dolor sit amet, </a><br>
                    <a href="#" class="footersub">consectetur adipiscing elit. </a><br>                 
                    <a href="#" class="footersub">ante luctus vel.</a><br>
                    <input class="form-control" id="inputdefault" type="text" placeholder="Email Address"><br>
                    <a class="subcribe" href="#"><p class="subcribetxt">Subscribe</p></a>

                </div>
                <div class="col-md-3 col-sm-3">
                    <p class="terms footermain">TERMS AND CONDITIONS</p>
                    
                    <p class="termsint">©2015 propertysearchsl.com</p>
                    <P class="termsint">All rights reserved.</P>
 <a href="http://wearedesigners.net/" target="_blank">
   <div>
    <link href='http://fonts.googleapis.com/css?family=Inika:400,700' rel='stylesheet' type='text/css'>
    <div class="foorter-sign">
        <div style="float: left;">
        <p class="footer-p1">website design and development</p>
        <p class="footer-p2">We Are Designers</p>
        </div>
        <div>
        <img src="<?php bloginfo('template_directory'); ?>/images/01.png">
        </div>
    </div>
</div>
</a>
                </div>
            </div>
    </div>
</div>

</footer>
