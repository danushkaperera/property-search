<?php
/* Template Name: Service*/

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
$post_155 = get_post(155); 
$title = $post_155->post_title;
$content = $post_155->post_content;

get_header(); ?>
 <style> 
.bnner-text{
     top: 285px;
   }

@media(min-width:320px) and (max-width: 700px){
 .back-white{
    height: auto!important;
    }
 }   
    body{
    background-color:#fff;
    }
/*this css need to move style.css*/
   .icon-col{
    text-align: center;
   }
.pad-bot-serv{
  padding-bottom: 40px;
}
.content-text-srev{
font-family: 'Lato', sans-serif !important;
font-weight: 500 !important;
color: #666666;
font-size: 14px;
padding-top: 10px;

}
.service-text{
font-family: 'Lato', sans-serif !important;
font-weight: 500 !important;
color: #666666;
font-size: 18px;
padding-top: 10px;
text-align: center;
}
.service-cont{
  padding-top: 20px;
}
.service-row{
  padding: 2em 0px 4em;
}
.service-row2{
 padding: 4em 0px 10px;
}

  </style>
<!-- Latest compiled and minified CSS -->
<div class="pad-top gray-back">
      <div class="content-fluid padding0  ">
             <div class=" banimgOne2"><img src="<?php echo get_field( 'banners',155 ); ?>" class="img-responsive"></div>
       </div>
 <div class="content-fluid padding0 back-white ">
	<div id="primary" class="container">
	      
<div class="col-md-12 content-about"> 					
      <p class="fproperty bold-txt" style="text-transform: uppercase;"><?php echo $title; ?></p>
	 		<p class="blueline">
         <img src="<?php bloginfo('template_directory'); ?>/images/carosel2/blueunderline.jpg">
      </p>

</div>

<div class="row">
  <div class="col-sm-12 service-row">
      <div class="col-sm-4 icon-col">
         <img src="<?php bloginfo('template_directory'); ?>/images/service/services_1.png" class="img-rounded ">
         <p class="service-text"> Relocation Services</p>
      </div>
      <div class="col-sm-4 icon-col">
         <img src="<?php bloginfo('template_directory'); ?>/images/service/services_2.png" class="img-rounded ">
            <p class="service-text">Property Consultation </p>
      </div>
      <div class="col-sm-4 icon-col">
         <img src="<?php bloginfo('template_directory'); ?>/images/service/services_3.png" class="img-rounded ">
            <p class="service-text">Legal Advice </p>
      </div>
    
  </div>
  
</div>

<div class="row service-row">
  <div class="col-sm-12">
      <div class="col-sm-4 icon-col">
         <img src="<?php bloginfo('template_directory'); ?>/images/service/services_4.png" class="img-rounded ">
            <p class="service-text"> Financial Advice</p>
      </div>
      <div class="col-sm-4 icon-col">
         <img src="<?php bloginfo('template_directory'); ?>/images/service/services_5.png" class="img-rounded ">
            <p class="service-text">Shipment of Goods </p>
      </div>
      <div class="col-sm-4 icon-col">
         <img src="<?php bloginfo('template_directory'); ?>/images/service/services_6.png" class="img-rounded ">
            <p class="service-text">Visa Consultation and more. </p>
      </div>
    
  </div>
  
</div>
<div class="container">
<div class="row">
<div class="col-md-12">           
      <p class="fproperty bold-txt">SERVICES IN THE SOUTH</p>
      <p class="blueline">
         <img src="<?php bloginfo('template_directory'); ?>/images/long-undrline.png">
      </p>
</div>
  <div class="col-sm-12 service-row2">
      <div class="col-sm-3 icon-col">
         <img src="<?php bloginfo('template_directory'); ?>/images/service/services_7.png" class="img-rounded ">
            <p class="service-text">Property Management  </p>
      </div>
      <div class="col-sm-3 icon-col">
         <img src="<?php bloginfo('template_directory'); ?>/images/service/services_8.png" class="img-rounded ">
            <p class="service-text"> Renovations </p>
      </div>
      <div class="col-sm-3 icon-col">
         <img src="<?php bloginfo('template_directory'); ?>/images/service/services_9.png" class="img-rounded ">
            <p class="service-text">Swimming Pool Construction </p>
      </div>

      <div class="col-sm-3 icon-col">
         <img src="<?php bloginfo('template_directory'); ?>/images/service/services_10.png" class="img-rounded ">
      </div>
       <p class="service-text">Construction </p>
  </div>
  </div>
    <div class="row service-row2">
        <p class="content-text"> We provide a full range of construction services for new builds and renovations, including architectural planning, final drawings, supply of construction crew and project managers. We can also assist you with obtaining local planning permissions as needed. </p>
        <p class="content-text">
        A comprehensive set of onsite project management services is offered to ensure the success of your construction project.
        </p>
    </div>

  <div class="row bullrt-point">
        <div class="col-sm-3 col-md-offset-2">
          <p class="content-text-srev"><img src="<?php bloginfo('template_directory'); ?>/images/service/rect1.png" alt="ico-squre" /> &nbsp&nbsp<span>Onsite Management</span> </p>
        </div>
        <div class="col-sm-3 ">
           <p class="content-text-srev"><img src="<?php bloginfo('template_directory'); ?>/images/service/rect1.png" alt="ico-squre" /> &nbsp&nbsp<span> Problem Resolution</span> </p>
        </div>
         <div class="col-sm-3 ">
           <p class="content-text-srev"><img src="<?php bloginfo('template_directory'); ?>/images/service/rect1.png" alt="ico-squre" />&nbsp&nbsp<span>Status Reporting</span> </p>
        </div>
    </div>



<div class="col-md-12 service-row">           
      <p class="fproperty bold-txt">SERVICES IN COLOMBO</p>
      <p class="blueline">
         <img src="<?php bloginfo('template_directory'); ?>/images/long-undrline.png">
      </p>
</div>


<div class="container">
  <div class="row pad-bot-serv">

   <div class="col-sm-12">
      <div class="col-xs-3 col-xs-offset-3 icon-col">
           <img src="<?php bloginfo('template_directory'); ?>/images/service/services_11.png" class="img-rounded ">
            <p class="service-text"> Property consultation  </p>
      </div>
      <div class="col-xs-3 icon-col">
           <img src="<?php bloginfo('template_directory'); ?>/images/service/services_12.png" class="img-rounded ">
            <p class="service-text"> Renovations </p>
      </div>
     
    
  </div>
    
  </div>
  
</div>


	</div><!-- .content-area -->

	</div>
 </div> 
</div>
<?php get_footer(); ?>
