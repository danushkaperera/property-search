<?php
/* 


Template Name: Rent

**/

get_header();
?>


 <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/swiper.min.css">
 <link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/css/custom.css">

    <!-- Demo styles -->
    <style>
   .address{
    	 padding-left: 14px;
    }

@media (max-width: 767px) and (min-width: 320px){
/*#mynav {
    top: -120px !important;
  }*/
.custom-nav {  
    position: relative;
     top: -11px!important;
    -webkit-transform: translate(0px, -120px);
    /*transform: translate(-50%, -50%);*/
}
  .move-next,.move-prev {
    top: 102% !important;
}
.move-prev {
    left: 140px !important;
 
   }
   .move-next {
    right: 140px !important;
 
   }

 
}
@media(min-width:768px) and (max-width: 1024px){
	 .navigationbar{
    	height:160px!important;
    }
    
     .rent-buy-pad {
    padding-top:75px !important;
}
.move-next, .move-prev {
    top: 49% !important;
}
    
    
  
}






    .navigationbar{
   	    height: 170px;
   }
   
     .main-nav{
        display:none;
    }
    .navxxx{
        display:block;
    }
    .search-bar {
    position: relative;
    top: 2px !important;
        
    }
    .logo{
        display:none;
    }

  
    .swiper-slide {
        text-align: center;
        font-size: 18px;
        background: #fff;
        height: 527px;
        
        /* Center slide text vertically */
      /*  display: -webkit-box;
        display: -ms-flexbox;
        display: -webkit-flex;
        display: flex;
        -webkit-box-pack: center;
        -ms-flex-pack: center;
        -webkit-justify-content: center;
        justify-content: center;
        -webkit-box-align: center;
        -ms-flex-align: center;
        -webkit-align-items: center;
        align-items: center;*/
    }
    
    
    
    </style>




<div class="pad-top ">
      <div class="content-fluid padding0 ">
             <div class=" banimgOne">
             	<img src="<?php echo get_field( 'banners',8 ); ?>" alt="baner" class="img-responsive">
             	 <p class="bnner-text"> PROPERTY LISTINGS</p>
             	</div>
           </div>
   </div>
<div class="container-fluid back-white" >
	<div class="container search-bar" >		
		      <div class="row ">
		      	  <div class="col-md-12 ">
		                <div id="bigsearch" class=" bigcol2 " >
		                   <a  href="<?php echo esc_url( home_url( '/home' ) ); ?>" rel="home" >
        		                   <div id="search-logo" class="col-sm-2 findlogo2 padd0">
        		                    <img src="<?php bloginfo('template_directory'); ?>/images/findprop.png" class="img-responsive" alt="findlogo">
        		                   </div>
        		             </a>
		                   <div class="col-sm-10 formsearch">  
							                 <form role="search" class="search-row" method="POST" id="searchform" action="<?php echo home_url( '/search-result' ); ?>">
							                  <?php
							                    $terms_proptype = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'property_type',
							                        'hide_empty' => false,
							                        ) );
							                        
							                          $terms_location = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'location',
							                        'hide_empty' => false,
							                        ) );
							                        
							                        
							                          $terms_beds = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'beds',
							                        'hide_empty' => false,
							                        ) );
							                        
							                        
							                        //   $terms_bathrooms = get_terms( array(
							                        // 'post_type' => 'property',
							                        // 'taxonomy' => 'bathrooms',
							                        // 'hide_empty' => false,
							                        // ) );
							                        
							                        
							                        //   $terms_sq_ft = get_terms( array(
							                        // 'post_type' => 'property',
							                        // 'taxonomy' => 'sq_ft',
							                        // 'hide_empty' => false,
							                        // ) );
							                        
							                        
							                          $terms_price = get_terms( array(
							                        'post_type' => 'property',
							                        'taxonomy' => 'price',
							                        'hide_empty' => false,
							                        ) );
							                        
							                        
							                        
							                        
							                   ?>
							                   
							                   
							       <div class=" row">		
										<div  class="col-sm-2 five-cols search-padd">
											<label class="lbl">Property Type</label>
												<select class="form-control height-fild padd0" name="proptype" >
													<option value="">any</option>
														     <?php  foreach($terms_proptype as $term ){   ?>
													<option value="<?php echo $term->name; ?>" ><?php echo $term->name; ?></option>
														      <?php } ?>
												</select>
							                  		    </div>
							           <div  class="col-sm-2 five-cols search-padd">
							                 <label class="lbl">Location</label>
												<select class="form-control height-fild padd0"  name="location" >
														      <option value="">any</option>
														     <?php  foreach($terms_location as $term2 ){   ?>
														      <option value="<?php echo $term2->name; ?>"><?php echo $term2->name; ?></option>
														      <?php } ?>
												 </select>
							           </div>
							            <div  class="col-sm-2 five-cols search-padd">
							                <label class="lbl">Beds</label>
												<select class="form-control height-fild padd0" name="beds" >
													<option value="">any</option>
														     <?php  foreach($terms_beds as $term3 ){   ?>
													 <option value="<?php echo $term3->name; ?>" ><?php echo $term3->name; ?></option>
														      <?php } ?>
												 </select>
							            </div>
							       <!--      <div  class="col-sm-2 seven-cols search-padd">
							                  			  	<label class="lbl">Baths</label>
														    <select class="form-control height-fild padd0" name="bathrooms">
														      <option value="">any</option>
														     <?php  //foreach($terms_bathrooms as $term4 ){   ?>
														      <option value="<?php //echo $term4->name; ?>" ><?php //echo $term4->name; ?></option>
														      <?php //} ?>
														    </select>
							             </div>
							            <div  class="col-sm-2 seven-cols search-padd">
							                  			  	<label class="lbl">Sq ft</label>
														    <select class="form-control padd0 height-fild" name="sq_ft">
														      <option value="">any</option>
														     <?php // foreach($terms_sq_ft as $term5){   ?>
														      <option value="<?php //echo $term5->name; ?>" ><?php// echo $term5->name; ?></option>
														      <?php //} ?>
														    </select>
							            </div> -->
							            <div  class="col-sm-2 five-cols search-padd">
							                  		  	   <label class="lbl">Price</label>
														    <select class="form-control padd0 height-fild" name="price">
														      <option value="">any</option>
														     <?php  foreach($terms_price as $term6 ){   ?>
														      <option value="<?php echo $term6->name; ?>" ><?php echo $term6->name; ?></option>
														      <?php } ?>
														    </select>
							            </div>
							            <div  class="col-sm-2 five-cols search-padd">
							                  			  <label class="lbl"></label>
							                  		 <input name="btn_search" type="submit" class="btn btn-primary" id="searchsubmit" value="Search" />
							                  	    </div>
							            </div>
							        </form>	
		                  
		                        </div>
		                    </div>
		       </div> 
	    </div> 

	
		<div class=" padd-prop " >	 
		  <div class="row">
		  		 <div class="col-md-12 rent-buy-pad">
		   		<p class="fproperty">FEATURED PROPERTIES</p>
		 		<p class="blueline"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/blueunderline.jpg"></p>
				 </div> 
		  </div>
		</div>	
  </div>
</div>	
	
	<div class="container">
	 <div class="row">
	  	<div class="col-md-12">
	        <div class="swiper-container">
	                    <div class="swiper-wrapper">
	                    	
					         <?php
					              $arg_main = array(
					            		'tax_query' => array(
										'post_type' => 'property',
										array(
										'taxonomy' => 'rent_or_buy',
										'field' => 'slug',
										'terms' => array( 'rent' )
										),
										 	array (									
									          'taxonomy' => 'property_type',
									          'terms' => array (),
										      'field' => 'slug',
										      'operator' => 'NOT IN',
										    ),
									)
					            );
					     
					            $the_query_main = new WP_Query($arg_main);
					        
					  
					            if ( $the_query_main->have_posts() ) :
					            while ( $the_query_main->have_posts() ) : $the_query_main->the_post();
					        	$args = array('orderby' => 'name', 'order' => 'ASC', 'fields' => 'all');
					            $term = wp_get_post_terms( $post->ID, 'property_type', $args );
					            		
					            ?> 
	               <div class="swiper-slide ">
				<?php 	if($term[0]->slug == 'land'){?>

	               	<div class="property-image">
								    <a href="<?php echo the_permalink();?>">
								    	<img src="<?php echo the_field('images');?>" class="img-responsive">
								    </a>
								</div>
						<div class="feature-home-prop">
									<div class="purpose <?php echo the_field('rent_buy'); ?>">
									<p class="buy-rent "><?php echo the_field('rent_buy'); ?></p>
									</div>
							<div class="propdetail">
									<div class="address">
										<p class="txt-address"> 
										<?php echo the_field('address'); ?>								
										</p>
									</div>
								<div class="price"><p class="txt-price"><?php echo the_field('price'); ?> </p>
									</div>
									
									<div class="row land-space">
										<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
											  <p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/home.jpg">
											   <span class="items-prop gray-color-font">Area</span>
											  </p>
											</div>
											<div class="col-xs-6 prop-item">
												<p class="items-txt"><?php echo the_field('lot_size'); ?></p>
											</div>
										</div>
									</div>
									
	                              	<!-- <div class="row">
	                              		<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
											     <p class="lot-size txt-align"><img src="<?php //bloginfo('template_directory'); ?>/images/carosel2/car.jpg">
										    	<span class="items-prop gray-color-font">Beds</span> </p>
										    </div>
									        <div class="col-xs-6 prop-item">
								        		<p class="items-txt gray-color-font "><?php// echo the_field('beds'); ?> </p>
									         </div>
									    </div>
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
												<p class="lot-size txt-align"><img src="<?php //bloginfo('template_directory'); ?>/images/carosel2/bathroom.jpg">
												<span class="items-prop gray-color-font">Bathrooms</span> </p>
											</div>
											<div class="col-xs-6 prop-item">
												<p class=" items-txt gray-color-font"><?php //echo the_field('bathrooms'); ?></p>
											</div>
										</div>
									</div> -->
									
	                               	<div class="col-xs-12 txt-address2">
									     <p class="gray-color-font" >
									      <img src="<?php bloginfo('template_directory'); ?>/images/carosel2/locationmark.jpg" /> <span><?php echo the_field('address'); ?> </span>
									      </p>
									</div>
                              </div>									
						</div>

<?php }else{?>


								<div class="property-image">
								    <a href="<?php echo the_permalink();?>">
								    	<img src="<?php echo the_field('images');?>" class="img-responsive">
								    </a>
								</div>
						<div class="feature-home-prop">
									<div class="purpose <?php echo the_field('rent_buy'); ?>">
									<p class="buy-rent "><?php echo the_field('rent_buy'); ?></p>
									</div>
							<div class="propdetail">
									<div class="address">
										<p class="txt-address"> 
										<?php echo the_field('address'); ?>								
										</p>
									</div>
									<div class="price"><p class="txt-price"><?php echo the_field('price'); ?> </p>
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
											  <p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/home.jpg">
											   <span class="items-prop gray-color-font">Squre Feet</span>
											  </p>
											</div>
											<div class="col-xs-6 prop-item">
												<p class="items-txt"><?php echo the_field('lot_size'); ?></p>
											</div>
										</div>
									</div>
									
	                              	<div class="row">
	                              		<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
											     <p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/car.jpg">
										    	<span class="items-prop gray-color-font">Beds</span> </p>
										    </div>
									        <div class="col-xs-6 prop-item">
								        		<p class="items-txt gray-color-font "><?php echo the_field('beds'); ?> </p>
									         </div>
									    </div>
									</div>
									
									<div class="row">
										<div class="col-xs-12">
											<div class="col-xs-6 prop-item">
												<p class="lot-size txt-align"><img src="<?php bloginfo('template_directory'); ?>/images/carosel2/bathroom.jpg">
												<span class="items-prop gray-color-font">Bathrooms</span> </p>
											</div>
											<div class="col-xs-6 prop-item">
												<p class=" items-txt gray-color-font"><?php echo the_field('bathrooms'); ?></p>
											</div>
										</div>
									</div>
									
	                               	<div class="col-xs-12 txt-address2">
									     <p class="gray-color-font" >
									      <img src="<?php bloginfo('template_directory'); ?>/images/carosel2/locationmark.jpg" /> <span><?php echo the_field('address'); ?> </span>
									      </p>
									</div>
                              </div>									
						</div>
							<?php }?>
					</div>
	                    <?php endwhile;?>
	            <?php endif;?>

					 	
				   </div>
				</div>
		<a class=" move-prev"><img src="<?php bloginfo('template_directory'); ?>/images/left-arrow.png"></img></a>
                <a class=" move-next "> <img src="<?php bloginfo('template_directory'); ?>/images/right-arrow.png"></img></a>
			
				 </div>
			</div>	    
        </div>	 
   

  

  <!-- Swiper JS -->
    <script src="<?php bloginfo('template_directory'); ?>/js/swiper.min.js"></script>

    <!-- Initialize Swiper -->
    <script>
    $(window).scroll(function (event) {   
    document.getElementById('hide').click();
    });
    var width = $(window).width();
    if(width < 480){ //mobile
    	 var swiper = new Swiper('.swiper-container', {
       nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 1,
        slidesPerColumn: 2,
        paginationClickable: true,
        spaceBetween: 30
    });
    
    }else if(width > 480 && width <= 769){ //tab
    	 var swiper = new Swiper('.swiper-container', {
        nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 2,
        slidesPerColumn: 2,
        paginationClickable: true,
        spaceBetween: 30
    });
    
    }else if(width > 769){
    	
    	 var swiper = new Swiper('.swiper-container', {
       nextButton:'.move-next',
        prevButton:'.move-prev',
        slidesPerView: 3,
        slidesPerColumn: 2,
        paginationClickable: true,
        spaceBetween: 30
    });
    
    }
    // var swiper = new Swiper('.swiper-container', {
    //     pagination: '.swiper-pagination',
    //     slidesPerView: 3,
    //     slidesPerColumn: 1,
    //     paginationClickable: true,
    //     spaceBetween: 30
    // });
    
    
      $("#show").click(function(){
        $("#show").hide();
         $("#hide").show();
         $('.navbar2').css({'display': 'block'});
    //    $("#navxxx").toggle("slow", "linear");
    });
    $("#hide").click(function(){
        $("#show").show();
         $("#hide").hide();
         $('.navbar2').css({'display': 'none'});
    });
    </script>
<?php get_footer(); ?>
