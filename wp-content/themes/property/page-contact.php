<?php
/* Template Name: Contact */

/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */
 
get_header();
?>
  <style>
  .wpcf7-submit{
    width: 131px; 
    margin-bottom: 17px;
  }
  
  @media (max-width: 700px) and (min-width: 320px){
.back-white {
    height: 510px!important;
}
.screen-reader-response{
    margin-bottom: -112px!important;
}
.padd-prop{
  padding-top: 83px!important;
}
}
.screen-reader-response{
 margin-bottom: -112px!important;
}
    .main-nav{
    	background-color:transparent !important;
    }
  </style>
	  

<div class="pad-top ">
      <div class="content-fluid padding0 ">             
             	     <div class=" banimgOne2"><img src="<?php echo get_field( 'banners',60); ?>" class="img-responsive"></div>
             	
             	</div>
           </div>
   </div>
<div class="container-fluid   padd-prop contact-padd " >
	<div class="container " >
		
		
	
		<?php
		// Start the loop.
		while ( have_posts() ) : the_post();

		the_content();
		// End the loop.
		endwhile;
		?>
		</div>
</div>

<?php get_footer(); ?>
