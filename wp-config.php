<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */
define ('WPCF7_AUTOP', false);
// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress propsearch*/
define('DB_NAME', 'propertysearchlive');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '83`xA)uIw^?dE^@ZGr dw7CW)y(?|i_t9kPe(mDepFr5~&T#9HR!rl}:XkM5efUV');
define('SECURE_AUTH_KEY',  'CNh3O7kLm.<gU}kuT,$:@=v+|e}e*f`/K28r61ns-gca+|(f2iG3xvhVM3DE%g/_');
define('LOGGED_IN_KEY',    'Za=XT(JsTVP@3G,=>no8%eF%@P]H25rjYvxyPkdWm$IB1=|~y|{xy_1 MTf_w(*[');
define('NONCE_KEY',        '#ASrz?bMp+F/BjL4UU<01H}mlM4M~{m&$<w-F*_JN%.;av`7)sbLMu+ejz{q`/S ');
define('AUTH_SALT',        'p?k?f-!qpU^0pJb8HQR|VxxfmOi2$2nVK7]oP8LzM/K2f&ykZ_ZH|&Fp>Lu*Uo#O');
define('SECURE_AUTH_SALT', 'd[R_N*^|($4~@x1|in*cgEmx~OLi5mP1Lkok]#}px/|UD|s!%{AsmC:)4|=rB#4b');
define('LOGGED_IN_SALT',   'pu+1RLCMrRt93Q0Xtndi?&1ir-os/bE4h9f3t&C $t{oq&{^r-f&i&WI>kz9~MjD');
define('NONCE_SALT',       '[,ScHiZSm{xD9*iCtjrx#2%pW>ghxsbzqrb}6X<Fy+Oy|Ug;|yRGhI-ME>t9Bsft');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
